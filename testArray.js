const { each, map, reduce, find, filter, flatten } = require("./array");

const testArray = [1, 2, 3, 4, 5, 5];

console.log("each() =>");
each(testArray, (e, i) => {
  console.log(`idx: ${i}, element: ${e}`);
});

console.log("\nmap() =>");
const mapResult = map(testArray, (e, i) => e * e);
console.log(mapResult);

console.log("\nreduce() =>");
const reduceResult = reduce(testArray, (x, y) => x + y, 100);
console.log(reduceResult);

console.log("\nfind() =>");
const findResult = find(testArray, (e) => e == 6);
console.log(findResult);

console.log("\nfilter() =>");
const filterResult = filter(testArray, (e) => e % 3 === 1);
console.log(filterResult);

console.log("\nflatten() =>");
const testFn = (t) => console.log("I am nested deep inside");
const testObj = { a: 1, b: 2 };
const nestedArray = [1, [2], [[3]], ["a", [32, [testFn], "cd"], ["as"], [testObj], []]];
const flattenResult = flatten(nestedArray);
console.log(flattenResult);
