const keys = (obj) => {
  let objKeys = [];
  if (obj && !Array.isArray(obj)) {
    for (i in obj) {
      objKeys.push(i);
    }
  }
  return objKeys;
};

const values = (obj) => {
  let objValues = [];
  if (obj && typeof obj === "object") {
    if (Array.isArray(obj)) return obj;
    for (i in obj) {
      objValues.push(obj[i]);
    }
  }
  return objValues;
};

const mapObject = (obj, cb) => {
  let newObj = {};
  if (obj && typeof obj === "object") {
    for (i of keys(obj)) {
      newObj[i] = cb(obj[i]);
    }
  }
  return newObj;
};

const pairs = (obj) => {
  let keyValues = [];
  if (obj) {
    for (i of keys(obj)) {
      keyValues.push([i, obj[i]]);
    }
  }
  return keyValues;
};

const invert = (obj) => {
  let newObj = {};
  if (obj && typeof obj === "object") {
    for (i of keys(obj)) {
      newObj["" + obj[i]] = i;
    }
  }
  return newObj;
};

const defaults = (obj, defaultProps) => {
  if (obj && typeof obj === "object" && typeof defaultProps === "object") {
    if (!Array.isArray(obj) && !Array.isArray(defaultProps)) {
      obj = { ...obj, ...defaultProps };
    }
  }
  return obj;
};

module.exports = {
  keys: keys,
  values: values,
  mapObject: mapObject,
  pairs: pairs,
  invert: invert,
  defaults: defaults,
};
