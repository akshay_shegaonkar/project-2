const { counterFactory, limitFunctionCallCount, cacheFunction } = require("./closures");

console.log("\ncounterFactory() =>");
const obj1 = counterFactory();
console.log(obj1.increment());
console.log(obj1.increment());
console.log(obj1.increment());
console.log(obj1.increment());
console.log(obj1.decrement());
console.log(obj1.decrement());

console.log("\ncounterFactory() =>");
function sheep() {
  console.log("Sheep");
}
const obj2 = limitFunctionCallCount(sheep, 5.6);
obj2();
obj2();
obj2();
obj2();
obj2();
obj2();
obj2();
obj2();

console.log("\ncacheFunction() =>");
const mult = function (n, m) {
  return n * m;
};
const obj3 = cacheFunction(mult);
console.log(obj3(5, 5));
console.log(obj3(10, 3));
console.log(obj3(5, 5));
console.log(obj3(10, 4));
