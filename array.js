const each = (elements, cb) => {
  if (elements && cb) {
    if (!Array.isArray(elements)) return;
    for (let i = 0; i < elements.length; i++) {
      cb(elements[i], i);
    }
  }
};

const map = (elements, cb) => {
  if (Array.isArray(elements)) {
    let newElements = [];
    for (let i = 0; i < elements.length; i++) {
      const cbResult = cb(elements[i], i);
      newElements.push(cbResult);
    }
    return newElements;
  }
};

const reduce = (elements, cb, startingValue) => {
  if (Array.isArray(elements) && cb) {
    let computedValue = startingValue;
    if (startingValue === undefined || startingValue === null) {
      computedValue = elements[0];
      elements = elements.splice(1);
    }
    each(elements, (e, i) => {
      computedValue = cb(computedValue, e);
    });
    return computedValue;
  }
};

const find = (elements, cb) => {
  if (Array.isArray(elements) && cb) {
    for (let i = 0; i < elements.length; i++) {
      if (cb(elements[i])) {
        return elements[i];
      }
    }
  }
};

const filter = (elements, cb) => {
  let newElements = [];
  if (Array.isArray(elements) && cb) {
    each(elements, (e, i) => {
      if (cb(e)) {
        newElements.push(e);
      }
    });
  }
  return newElements;
};

const flatten = (elements) => {
  let newArray = [];
  if (Array.isArray(elements)) {
    each(elements, (e, i) => {
      if (Array.isArray(e)) {
        if (e.length > 0) {
          newArray = [...newArray, ...flatten(e)];
        }
      } else {
        newArray.push(e);
      }
    });
  }
  return newArray;
};

module.exports = {
  each: each,
  map: map,
  reduce: reduce,
  find: find,
  filter: filter,
  flatten: flatten,
};
