const counterFactory = () => {
  let counter = 0;
  return {
    increment: () => {
      counter += 1;
      return counter;
    },
    decrement: () => {
      counter -= 1;
      return counter;
    },
  };
};

const limitFunctionCallCount = (cb, n) => {
  if (cb) {
    let counter = 0;
    let fn = () => cb();
    if (n && typeof n === "number") {
      counter = parseInt(n);
      fn = () => {
        if (counter > 0) {
          cb();
        } else {
          console.log("Function call limit reached.");
        }
        counter -= 1;
      };
    }
    return fn;
  }
};

const cacheFunction = (cb) => {
  let cache = {};
  return function () {
    args = "" + [...arguments];
    if (!cache[args]) {
      cache[args] = cb(...arguments);
    }
    return cache[args];
  };
};

module.exports = {
  counterFactory: counterFactory,
  limitFunctionCallCount: limitFunctionCallCount,
  cacheFunction: cacheFunction,
};
