const { keys, values, mapObject, pairs, invert, defaults } = require("./objects");

const testObject = { name: "Bruce Wayne", age: 36, location: "Gotham" };

// console.log("keys() =>");
// console.log(keys(testObject));

console.log("values() =>");
console.log(values([1, 2, 3, 4, "asd"]));

// console.log("mapObject() =>");
// console.log(mapObject(testObject, (v) => "hi " + v));

// console.log("pairs() =>");
// console.log(pairs(testObject));

// console.log("invert() =>");
// console.log(invert(testObject));

// console.log("defaults() =>");
// console.log(defaults(testObject, { name: "Tony Stark", age: 46, city: "NY" }));
